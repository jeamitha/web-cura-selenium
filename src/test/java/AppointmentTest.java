import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AppointmentTest {
    WebDriver driver;

    @BeforeClass
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Lawencon\\IdeaProjects\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void logout()throws InterruptedException {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(1000);
    }

    @Test
    public void TC01(){
        //go to url
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Doe");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("ThisIsNotAPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        //input field appointment
        Select ComboFacility = new Select(driver.findElement(By.id("combo_facility")));
        ComboFacility.selectByVisibleText("Hongkong CURA Healthcare Center");

        //input health program
        driver.findElement(By.id("radio_program_medicare")).click();

        //input date
        driver.findElement(By.id("txt_visit_date")).click();
        driver.findElement(By.xpath("/html/body/div/div[1]/table/tbody/tr[4]/td[4]")).click();

        //input comment
        driver.findElement(By.xpath("//*[@id=\"txt_comment\"]")).sendKeys("testing");

        //click button
        driver.findElement(By.xpath("//*[@id=\"btn-book-appointment\"]")).click();

        //ekspetasi
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"summary\"]/div/div/div[1]/h2")).getText(), "Appointment Confirmation");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"facility\"]")).getText(),"Hongkong CURA Healthcare Center");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"hospital_readmission\"]")).getText(), "No");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"program\"]")).getText(),"Medicare");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"visit_date\"]")).getText(), "19/10/2022");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"comment\"]")).getText(),"testing");
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException{
        Thread.sleep(2000);
        driver.quit();
    }


}
