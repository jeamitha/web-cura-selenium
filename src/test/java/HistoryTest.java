import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class HistoryTest {
        WebDriver driver;

        @BeforeClass
        public void setup(){
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\Lawencon\\IdeaProjects\\chromedriver_win32\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

        @Test
        public void TC01() {
        //go to url
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Doe");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("ThisIsNotAPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        //input field appointment
        Select ComboFacility = new Select(driver.findElement(By.id("combo_facility")));
        ComboFacility.selectByVisibleText("Hongkong CURA Healthcare Center");

        //input health program
        driver.findElement(By.id("radio_program_medicare")).click();

        //input date
        driver.findElement(By.id("txt_visit_date")).click();
        driver.findElement(By.xpath("/html/body/div/div[1]/table/tbody/tr[4]/td[4]")).click();

        //input comment
        driver.findElement(By.xpath("//*[@id=\"txt_comment\"]")).sendKeys("testing");

        //click button
        driver.findElement(By.xpath("//*[@id=\"btn-book-appointment\"]")).click();

        //go to History Menu
        driver.findElement(By.xpath("//*[@id=\"menu-toggle\"]/i")).click();
        driver.findElement(By.xpath("//*[@id=\"sidebar-wrapper\"]/ul/li[3]/a")).click();

        //ekspetasi
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"history\"]/div/div[2]/div/div/div[1]")).getText(), "19/10/2022");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"facility\"]")).getText(), "Hongkong CURA Healthcare Center");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"hospital_readmission\"]")).getText(), "No");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"history\"]/div/div[2]/div/div/div[2]/div[8]")).getText(), "Medicare");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"comment\"]")).getText(), "testing");
        }

        @AfterMethod
        public void logout()throws InterruptedException {
            driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
            Thread.sleep(1000);
        }

        @AfterClass
        public void closeBrowser() throws InterruptedException{
                Thread.sleep(2000);
                driver.quit();
        }

}
