import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginTest {

    WebDriver driver;

    @BeforeClass
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Lawencon\\IdeaProjects\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void logout()throws InterruptedException {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(1000);
    }

    @Test
    public void TC01(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        //ambil data value username & password
        String username = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");

        //login sukses dengan memasukan data value
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        //ekspetasi: menuju halaman berjudul "Make Appointment"
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(), "Make Appointment");
    }

    @Test
    public void TC02(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        //login menggunakan password salah
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Doe");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("hehe");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        //ekspetasi: muncul error message
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @Test
    public void TC03(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        //login menggunakan username salah
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Die");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("ThisIsNotAPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        //ekspetasi: muncul error message
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @Test
    public void TC04(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        //login menggunakan username dan passsword salah
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Die");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("ThisNotAPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        //ekspetasi: muncul error message
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException{
        Thread.sleep(2000);
        driver.quit();
    }
}
